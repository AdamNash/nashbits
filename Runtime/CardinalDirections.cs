using UnityEngine;

namespace NashBits
{
    public enum CardinalDirection
    {
        None,
        North,
        East,
        West,
        South
    }
    
    
    public static class CardinalDirections
    {
        public static CardinalDirection ToCardinalDirection (this Vector2 v)
        {
            const float DOT_THRESH = 0.7f;
            
            if (Vector2.Dot(v, Vector2.down) > DOT_THRESH)
                return CardinalDirection.South;

            if (Vector2.Dot(v, Vector2.right) > DOT_THRESH)
                return CardinalDirection.East;

            if (Vector2.Dot(v, Vector2.left) > DOT_THRESH)
                return CardinalDirection.West;
            
            if (Vector2.Dot(v, Vector2.up) > DOT_THRESH)
                return CardinalDirection.North;

            return CardinalDirection.None;
        }
        
        
        public static Vector2 ToVector2 (this CardinalDirection direction)
        {
            return direction switch
            {
                CardinalDirection.North => Vector2.up,
                CardinalDirection.East => Vector2.right,
                CardinalDirection.West => Vector2.left,
                CardinalDirection.South => Vector2.down,
                _ => Vector2.zero
            };
        }

        
        public static CardinalDirection ClockwiseAngleToCardinalDirection (float clockwiseAngle)
        {
            if (clockwiseAngle >= 315f || clockwiseAngle <= 45f)
                return CardinalDirection.North;
            
            if (clockwiseAngle < 135f)
                return CardinalDirection.East;
            
            if (clockwiseAngle < 225f)
                return CardinalDirection.South;

            if (clockwiseAngle < 315f)
                return CardinalDirection.West;

            return CardinalDirection.None;
        }
    }
}