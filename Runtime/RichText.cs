namespace NashBits
{
    public static class RichText
    {
        public static string RTBold (this string s)
        {
            return $"<b>{s}</b>";
        }

        public static string RTItalic (this string s)
        {
            return $"<i>{s}</i>";
        }

        public static string RTColor (this string s, string color)
        {
            return $"<color={color}>{s}</color>";
        }

        public static string RTSize (this string s, int size)
        {
            return $"<size={size}>{s}</size>";
        }
    }
}