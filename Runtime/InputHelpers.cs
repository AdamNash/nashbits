using UnityEngine;
using UnityEngine.InputSystem;

namespace NashBits
{
    public static class InputHelpers
    {
        /// <summary>
        /// If a num key was pressed this frame, returns it (0-9), otherwise returns -1
        /// </summary>
        public static int GetNumKeyDown ()
        {
            if (Keyboard.current.digit0Key.wasPressedThisFrame)
                return 0;
            
            if (Keyboard.current.digit1Key.wasPressedThisFrame)
                return 1;
            
            if (Keyboard.current.digit2Key.wasPressedThisFrame)
                return 2;
            
            if (Keyboard.current.digit3Key.wasPressedThisFrame)
                return 3;
            
            if (Keyboard.current.digit4Key.wasPressedThisFrame)
                return 4;
            
            if (Keyboard.current.digit5Key.wasPressedThisFrame)
                return 5;
            
            if (Keyboard.current.digit6Key.wasPressedThisFrame)
                return 6;
            
            if (Keyboard.current.digit7Key.wasPressedThisFrame)
                return 7;
            
            if (Keyboard.current.digit8Key.wasPressedThisFrame)
                return 8;
            
            if (Keyboard.current.digit9Key.wasPressedThisFrame)
                return 9;

            return -1;
        }
    }
}