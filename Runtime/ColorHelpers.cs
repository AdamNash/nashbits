using UnityEngine;

namespace NashBits
{
    public static class ColorHelpers
    {
        public static Color SetAlpha (this Color color, float alpha)
        {
            Color c = color;
            c.a = alpha;
            return c;
        }

        /// <summary>
        /// Lerps from color to white by amount
        /// </summary>
        public static Color Brighten (this Color color, float amount)
        {
            float a = color.a;
            Color c = Color.Lerp(color, Color.white, amount);
            c.a = a;
            return c;
        }

        /// <summary>
        /// Lerps from color and black by amount
        /// </summary>
        public static Color Darken (this Color color, float amount)
        {
            float a = color.a;
            Color c = Color.Lerp(color, Color.black, amount);
            c.a = a;
            return c;
        }
    }
}