using UnityEngine;

namespace NashBits
{
    [AddComponentMenu("NashBits/Spring Follow")]
    public class SpringFollow : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] Vector3 offset;
        [SerializeField] float targetPosSpeed = 1f;
        [SerializeField] float dampingRatio = 0.5f;
        [SerializeField] float angularFreq = Mathf.PI * 2f;

        Vector3 targetPos;
        
        float x;
        float y;

        float vx;
        float vy;

        
        
#if UNITY_EDITOR
        [Header("Gizmos")] 
        [SerializeField] bool drawGizmo = true;
        void OnDrawGizmos ()
        {
            if (!drawGizmo)
                return;
            
            Gizmos.color = Color.magenta;
            if (Application.isPlaying)
            {
                Gizmos.DrawSphere(targetPos, 0.25f);
                Gizmos.DrawLine(transform.position, targetPos);
            }
            else
            {
                if (target != null)
                {
                    targetPos = target.transform.position + offset;
                    Gizmos.DrawLine(transform.position, targetPos);
                }
            }
        }
#endif

        

        void Awake ()
        {
            x = transform.position.x;
            y = transform.position.y;
        }



        void Update ()
        {
            if (target == null)
                return;

            Vector3 tp = target.transform.position + offset;
            targetPos = Vector3.MoveTowards(targetPos, tp, targetPosSpeed * Time.deltaTime);

            NumericSpring.Spring(ref x, ref vx, targetPos.x, dampingRatio, angularFreq, Time.deltaTime);
            NumericSpring.Spring(ref y, ref vy, targetPos.y, dampingRatio, angularFreq, Time.deltaTime);

            transform.position = new Vector3(x, y);
        }
    }
}