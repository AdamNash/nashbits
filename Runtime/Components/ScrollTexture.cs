﻿using UnityEngine;

namespace NashBits
{
    [AddComponentMenu("NashBits/Scroll Texture")]
    [RequireComponent(typeof(Renderer))]
    public class ScrollTexture : MonoBehaviour
    {
        [SerializeField] Vector2 scrollSpeed = Vector2.right;

        Renderer rend;
        int mainTexID;


        void Awake ()
        {
            rend = GetComponent<Renderer>();
            mainTexID = Shader.PropertyToID("_MainTex");
        }


        void Update ()
        {
            float x = Mathf.Repeat(Time.time * scrollSpeed.x, 1f);
            float y = Mathf.Repeat(Time.time * scrollSpeed.y, 1f);
            rend.sharedMaterial.SetTextureOffset(mainTexID, new Vector2(x, y));
        }
    }
}