using UnityEngine;

namespace NashBits
{
    [AddComponentMenu("NashBits/Sine Movement")]
    public class SineMovement : MonoBehaviour
    {
        [SerializeField] float speed = 1f;
        [SerializeField] float amplitude = 1f;
        [SerializeField] Vector3 axis = Vector2.up;

        Vector3 startPos;
        
        
#if UNITY_EDITOR
        void OnDrawGizmosSelected ()
        {
            Gizmos.color = Color.yellow.SetAlpha(0.1f);
            
            Vector3 pos = transform.position;
            if (Application.isPlaying)
                pos = startPos;
            
            Gizmos.DrawSphere(pos, amplitude);
        }
#endif


        void Start ()
        {
            startPos = transform.position;
        }

        
        void Update ()
        {
            transform.position = startPos + (axis * (Mathf.Sin(Time.time * speed) * amplitude));
        }
    }
}