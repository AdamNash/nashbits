﻿using UnityEngine;

namespace NashBits
{
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T instance;
        
        protected bool singletonInitialized { get; private set; } // check to prevent things from happening before Awake, e.g. the SceneManager.sceneLoaded event;


        protected virtual void Awake() 
        {
            if (instance == null)
            {
                instance = this as T;
                DontDestroyOnLoad(gameObject);
                singletonInitialized = true;
            }
            else if (instance != this)
            {
                Destroy(gameObject); 
            }
        }


        protected virtual void OnDestroy ()
        {
            if (instance == this && singletonInitialized)
                instance = null;
        }
    }
}