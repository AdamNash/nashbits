// http://allenchou.net/2015/04/game-math-precise-control-over-numeric-springing/
// http://allenchou.net/2015/04/game-math-numeric-springing-examples/
// http://allenchou.net/2015/04/game-math-more-on-numeric-springing/

using UnityEngine;

namespace NashBits
{
    public static class NumericSpring
    {
        /// <summary>
        /// Moves from current value to target value with spring-like oscillation.
        /// <para> Uses the implicit euler method (always stable). </para>
        /// <para> Call each frame using refs to value and velocity.</para>
        /// </summary>
        /// <param name="x"> current value </param>
        /// <param name="v"> current velocity ref </param>
        /// <param name="xt"> target value </param>
        /// <param name="zeta"> damping ratio (0 = oscillates forever,  0-1 = oscillation descreases over time,  1+ = no oscillation, just drag) </param>
        /// <param name="omega"> angular frequency in radians/second (2Pi = 1Hz = 1 full period in 1 second) </param>
        /// <param name="h"> time step </param>
        public static void Spring (ref float x, ref float v, float xt, float zeta, float omega, float h)
        {
            float f = 1.0f + 2.0f * h * zeta * omega;
            float oo = omega * omega;
            float hoo = h * oo;
            float hhoo = h * hoo;
            float detInv = 1.0f / (f + hhoo);
            float detX = f * x + h * v + hhoo * xt;
            float detV = v + hoo * (xt - x);

            x = detX * detInv;
            v = detV * detInv;
        }



        /// <summary>
        /// Like Spring(), but uses the semi-implicit euler method (more efficient, but not always stable with very large zeta and omega).
        /// </summary>
        /// <param name="x"> current value </param>
        /// <param name="v"> current velocity ref </param>
        /// <param name="xt"> target value </param>
        /// <param name="zeta"> damping ratio (0 = oscillates forever,  0-1 = oscillation descreases over time,  1+ = no oscillation, just drag) </param>
        /// <param name="omega"> angular frequency in radians/second (2Pi = 1Hz = 1 full period in 1 second) </param>
        /// <param name="h"> time step </param>
        public static void SpringSemiImplicitEuler (ref float x, ref float v, float xt, float zeta, float omega, float h)
        {
            v += -2.0f * h * zeta * omega * v + h * omega * omega * (xt - x);
            x += h * v;
        }



        /// <summary>
        /// Like Spring(), but the oscillation decreases over the specified half-life (lambda) instead of a damping ratio.
        /// </summary>
        /// <param name="x"> current value </param>
        /// <param name="v"> current velocity ref </param>
        /// <param name="xt"> target value </param>
        /// <param name="omega"> angular frequency in radians/second (2Pi = 1Hz = 1 full period in 1 second) </param>
        /// <param name="h"> time step </param>
        /// <param name="lambda"> half-life (1 = oscillation halves every 1 second) </param>

        public static void SpringByHalfLife (ref float x, ref float v, float xt, float omega, float h, float lambda)
        {
            float zeta = -Mathf.Log(0.5f) / (omega * lambda);
            Spring(ref x, ref v, xt, zeta, omega, h);
        }
    }
}