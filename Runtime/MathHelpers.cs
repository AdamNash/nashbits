﻿using UnityEngine;

namespace NashBits
{
    public static class MathHelpers
    {
        /// <summary>
        /// Remaps a value from one range to another (e.g. 0.5(0,1) --> 50(0,100)
        /// </summary>
        public static float Remap (float v, float fromMin, float fromMax, float toMin, float toMax)
        {
            float normal = Mathf.InverseLerp(fromMin, fromMax, v);
            return Mathf.Lerp(toMin, toMax, normal);
        }

        /// <summary>
        /// Wraps a value within a range and handles negatives (e.g. i-2, 0, array.length-1 to skip backwards by 2 through an array and wrap to the top)
        /// </summary>
        public static int Wrap (int i, int min, int max)  
        {
            int range = max - min;
            
            // handles negative values: 
            if (i < min)
                return max - (min - i) % range;
            
            return min + (i - min) % range;
        }
        
        /// <summary>
        /// Angles are modular (e.g. 450 is the same value as 90), so you have to clamp the value AND mod the range
        /// </summary>
        public static float ClampModular (float val, float min, float max, float rangeMin = -180f, float rangeMax = 180f) 
        {
            float range = Mathf.Abs(rangeMax - rangeMin);
            
            if ((val %= range) < 0f) 
                val += range;
            
            return Mathf.Clamp(val + Mathf.Min(rangeMin, rangeMax), min, max);
        }
        
        
        public static float SignedAngleToClockwiseAngle (float signedAngle)
        {
            float clockwiseAngle = signedAngle;
            if (signedAngle < 0f)
                clockwiseAngle = 360f - Mathf.Abs(signedAngle);

            return clockwiseAngle;
        }
    }
}