using System;

namespace NashBits
{
    /// <summary>
    /// Create a class for a game event - e.g. public class TestEvent : GameEvent<TestEvent> { }
    /// Subscribe and unsubscribe with TestEvent.AddListener(OnTestEventFunc) and TestEvent.RemoveListener(OnTestEventFunc)
    /// Invoke with TestEvent.Invoke(new TestEvent())
    /// Add fields to the event class and pass data in the constructor on invoke - e.g. DamageEvent.Invoke(new DamageEvent(dmg))
    /// </summary>
    
    public abstract class GameEvent<T> where T : GameEvent<T>
    {
        public static event Action<T> onEvent;

        
        public static void AddListener (Action<T> action)
        {
            onEvent += action;
        }

        
        public static void RemoveListener (Action<T> action)
        {
            onEvent -= action;
        }

        
        public static void InvokeEvent (T eventData)
        {
            onEvent?.Invoke(eventData);
        }
    }
}